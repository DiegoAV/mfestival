# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This is a test of a territorium life vacancy

### How do I get set up? ###

* Configuration
* install composer and a local server, I use Laragon
* make: Composer update

* Database configuration
* in .env file modify these variables according yours DB_DATABASE=mfestival DB_USERNAME=root DB_PASSWORD=''
* make: php artisan migrate
