<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\guest;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users_egistered =  guest::orderBy('name','asc')->get();
        $more_data = compact('users_egistered');
        return view('home')->with($more_data);
    }
}
