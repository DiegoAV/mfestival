CREATE TABLE guests
(
  id             INT UNSIGNED AUTO_INCREMENT
    PRIMARY KEY,
  name           VARCHAR(255) NOT NULL,
  firstLastName  VARCHAR(255) NOT NULL,
  secondLastName VARCHAR(255) NULL,
  email          VARCHAR(255) NOT NULL,
  celphone       VARCHAR(255) NULL,
  age            TINYINT      NULL,
  cameFrom       VARCHAR(255) NULL,
  opinions       TEXT         NULL,
  created_at     TIMESTAMP    NULL,
  updated_at     TIMESTAMP    NULL,
  CONSTRAINT guests_email_unique
  UNIQUE (email)
)
  ENGINE = InnoDB
  COLLATE = utf8mb4_unicode_ci;

CREATE TABLE migrations
(
  id        INT UNSIGNED AUTO_INCREMENT
    PRIMARY KEY,
  migration VARCHAR(255) NOT NULL,
  batch     INT          NOT NULL
)
  ENGINE = InnoDB
  COLLATE = utf8mb4_unicode_ci;

CREATE TABLE password_resets
(
  email      VARCHAR(255) NOT NULL,
  token      VARCHAR(255) NOT NULL,
  created_at TIMESTAMP    NULL
)
  ENGINE = InnoDB
  COLLATE = utf8mb4_unicode_ci;

CREATE INDEX password_resets_email_index
  ON password_resets (email);

CREATE TABLE users
(
  id             INT UNSIGNED AUTO_INCREMENT
    PRIMARY KEY,
  name           VARCHAR(255) NOT NULL,
  email          VARCHAR(255) NOT NULL,
  password       VARCHAR(255) NOT NULL,
  remember_token VARCHAR(100) NULL,
  created_at     TIMESTAMP    NULL,
  updated_at     TIMESTAMP    NULL,
  CONSTRAINT users_email_unique
  UNIQUE (email)
)
  ENGINE = InnoDB
  COLLATE = utf8mb4_unicode_ci;


