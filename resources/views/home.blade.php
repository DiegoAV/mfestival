@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">People Registered</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                        <div class="row">
                            <div class="col-md-6">
                                <div class="input-group" style="width: 100%;">
                                    <input type="text"  class="form-control" id="nameFilter" placeholder="Name">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="input-group" style="width: 100%;">
                                    <input type="text"  class="form-control" id="fromFilter" placeholder="City">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <table class="table">
                                <thead>
                                <tr class="titles">
                                    <th scope="col">Name</th>
                                    <th scope="col">Age</th>
                                    <th scope="col">From</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Celphone</th>
                                    <th scope="col">Opinions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($users_egistered as $user)
                                    <tr>
                                        <td class="uname" >{{$user->name.' '.$user->firstLastName.' '.$user->seconLastName}}</td>
                                        <td>{{$user->age}}</td>
                                        <td class="from">{{$user->cameFrom}}</td>
                                        <td>{{$user->email}}</td>
                                        <td>{{$user->celphone}}</td>
                                        <td>{{$user->opinions}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{ asset('js/jquery.js') }}"></script>
<script type="text/javascript">
    $( document ).ready(function() {
        $('body').css('backgroundColor','#7EC0EE');

        function applyFilters(selector,intext){
            $(''+selector+'').closest('tr').removeClass('found hidden').filter(function (index) {
                return $( this ).html().toUpperCase().indexOf(intext.toUpperCase()) != -1
            }).closest('tr').addClass('found')

            $('tr').not('.found, .titles').addClass('hidden')
        }

        $('#nameFilter').keyup(function(event){
            if($('#fromFilter').val() != '') $('#fromFilter').val('')
            var intext = $(this).val()
            applyFilters('.uname',intext);
        })

        $('#fromFilter').keyup(function(event){
            if($('#nameFilter').val() != '') $('#nameFilter').val('')
            var intext = $(this).val()
            applyFilters('.from',intext);
        })


    });
</script>
@endsection
