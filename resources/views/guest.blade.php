@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default" style="background:rgba(128,128,128,0.5); color:#fff">
                    <div class="panel-heading" style="background-color:transparent; color:#fff">Join Us!</div>

                    <div class="panel-body">
                        <form class="form-horizontal">
                            {{ csrf_field() }}

                            <div class="form-group">
                                <label for="name" class="col-md-4 control-label">Name *</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="lastName1" class="col-md-4 control-label">Last Name 1 *</label>

                                <div class="col-md-6">
                                    <input id="lastName1" type="text" class="form-control" name="firstLastName" value="{{ old('firstLastName') }}" required autofocus>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="lastName2" class="col-md-4 control-label">Last Name 2</label>

                                <div class="col-md-6">
                                    <input id="lastName2" type="text" class="form-control" name="secondLastName" value="{{ old('secondLastName') }}" autofocus>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="celphone" class="col-md-4 control-label">Age</label>

                                <div class="col-md-6">
                                    <input id="age" type="number" class="form-control" name="age" max="999" value="{{ old('age') }}" autofocus>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="celphone" class="col-md-4 control-label">Celphone</label>

                                <div class="col-md-6">
                                    <input id="celphone" type="number" class="form-control" name="celphone" value="{{ old('celphone') }}" autofocus>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="cameFrom" class="col-md-4 control-label">Came From</label>

                                <div class="col-md-6">
                                    <input id="cameFrom" type="text" class="form-control" name="cameFrom" value="{{ old('cameFrom') }}" autofocus>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="opinions" class="col-md-4 control-label">Opinions</label>

                                <div class="col-md-6">
                                    <textarea id="opinions" placeholder="tell us why you wanna come?" rows="5" class="form-control" name="opinions" value="{{ old('opinions') }}" autofocus></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="email" class="col-md-4 control-label">E-Mail Address *</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4" style="display: flex; justify-content: center;">
                                    <button type="button" class="add_guest btn btn-primary">
                                        Register
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="{{ asset('js/jquery.js') }}"></script>

    <script type="text/javascript">
        $( document ).ready(function() {
            $(".add_guest").unbind('click').on('click',function(){
                 var data = $('.form-horizontal').serialize()
                 $.ajax({
                     type:'POST',
                     url:'guest/create',
                     data: data,
                     success:function(response){
                         if(response.status == 201){
                             alert('You are now registered to the festival of the year!')
                             window.location.href = "/";
                         }else{
                             alert('there is an error with your resgistration, try again or contact us!')
                         }
                     },
                     fail:function(response){
                         console.log(response)
                     }
                 });
            });

            $('body').css('backgroundImage','url(images/wall3.jpg)');
        });

    </script>
@endsection


